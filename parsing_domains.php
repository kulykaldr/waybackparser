<?php

require_once __DIR__ . '/input_settings.php';

// Очищаем список ключей от дублей
$searchKeysList = array_unique($searchKeysList);
file_force_contents($searchKeysListPath, $searchKeysList);

// Берем первый ключ из списка и записываем его в конец списка
$searchKey = array_shift($searchKeysList);
$searchKeysList[] = $searchKey;
file_force_contents($searchKeysListPath, $searchKeysList);

// Send request to webarchive
$url = 'https://web.archive.org/__wb/search/anchor?q=' . urlencode($searchKey);
try {
    $html = file_get_contents($url);
} catch (Exception $e) {
    sendToLog('Error request to webarchive' . $url, 'error');
    exit();
}

// Check for empty JSON
if (empty($html)) {
    sendToLog('Json is empty - ' . $url, 'error');
    exit();
}

$json_list = json_decode($html);

// Если json пустой вообще значит выкидываем этот логин и останавливаем скрипт
if (!$json_list) {
    sendToLog('Error parsing json', 'error');
    exit();
}

// Перебор доменов из выдачи
foreach ($json_list as $json) {
    $domain = strtolower(trim($json->name));
    $domainPages = (int)$json->webpage;

    // Check for working site. If don't work that's fine
    try {
        $response = $guzzle->get($domain);
    } catch (Exception $e) {
    }

    // If was error on request we will write domain to database
    // If not - check for redirects
    if (isset($response) && $response->getHeaderLine('X-Guzzle-Redirect-Status-History') === '301') {
        $redirect = $response->getHeaderLine('X-Guzzle-Redirect-History');
        $redirectDomain = strtolower(parse_url($redirect, PHP_URL_HOST));

        if ($domain === $redirectDomain) {
            sendToLog('Redirects to the same domain', 'error');
            continue;
        }
    }

    // If response status 200 - skip domain
    if (isset($response) && $response->getStatusCode() === 200) {
        sendToLog('Domain is still work', 'error');
        continue;
    }

    // If site have less than 100 pages - skip this site
    if ($domainPages < 100) {
        sendToLog('Domain has less then 100 pages', 'error');
        continue;
    }

    // Check black list
    if (selectFromDb($dbUrls, 'blacklist', $domain)) {
        sendToLog('Domain in black list', 'error');
        continue;
    }

    sendToLog('Add domain to domains list');
    insertToDb($dbUrls, 'domains', $domain);
}

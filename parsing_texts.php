<?php

use GuzzleHttp\Exception\RequestException;

require_once __DIR__ . '/input_settings.php';

$countUrls = $_REQUEST['count'] ?? 10;

while ($countUrls !== 0) {
    // Берем первую ссылку, а потом обновляем файл
    $urlData = selectFromDb($dbUrls, 'urls');
    $urlId = $urlData['id'];
    $url = $urlData['data'];

    if (empty($url)) {
        sendToLog('Url list is empty. Stop work', 'error');
        exit();
    }

    // Delete url from database
    deleteFromDbById($dbUrls, 'urls', $urlId);

    if (isset($domain) && $domain !== strtolower(parse_url($url, PHP_URL_HOST))) {
        $lang = false;
    }
    $url = 'http://wildtrees.ru/content/view/195/244';
    $domain = strtolower(parse_url($url, PHP_URL_HOST));
    $url = 'https://web.archive.org/web/' . $url;

    //////////////////////// GOOSE - START ///////////////////////////////
    // Определяем язык парсинга
    // Если язык не найден то пропускаем ссылку
    if (empty($lang)) {
        $lang = getLang($url);
        if (empty($lang)) {
            continue;
        }
    }

    // Устанавливаем язык
    $goose->config->set('language', $lang);

    // Парсим контент с помощью Goose
    try {
        $article = $goose->extractContent($url);
    } catch (RequestException $e) {
        sendToLog($e->getMessage() . ' - ' . $url, 'error');
    }

    // Если есть ошибки 400 или еще какие то пропускаем такую страницу
    if ($article->getRawResponse()->getStatusCode() !== 200) {
        continue;
    }

    // Получаем html
    $html = $article->getRawHtml();
    // Получаем Н1
    $h1 = getH1($html);
    // Получаем title
    $title = $article->getTitle();
    // Получаем статью
    $articleText = trim($article->getCleanedArticleText());

    // Меняем директорию записи для того чтобы понимать какой скрипт спарсил текст
    if (empty($articleText)) {
        sendToLog('Goose don\'t parsed text from - ' . $url, 'error');
    } else {
        $engine = 'goose';
        sendToLog('Goose successfully parsed text from - ' . $url, 'success');
    }
    //////////////////////// GOOSE - END ///////////////////////////////


    //////////////////////// BOILER - START ///////////////////////////////
    // Или нет спарсенного контента, то парсим с помощью boiler
    if (empty($articleText)) {

        // Достам текст
        $html = convertToUtf8($html);
        $articleText = trim($boiler->getContent($html));

        // Если boiler не спарсил контент, то пропускаем
        if (empty($articleText)) {
            sendToLog('Boiler don\'t parsed text from - ' . $url, 'error');
            continue;
        }

        $engine = 'boiler';
    }
    //////////////////////// BOILER - END ///////////////////////////////

    // определяем название файла
    $fileName = URLify::filter($h1 ?? $title, 60, '', true);
    if (empty($fileName)) {
        $fileName = date('d-m-Y-H-i-s') . generateRandomString(10);
    }
    $content = '';
    $content .= '#url#' . $url . '#/url#' . PHP_EOL;
    $content .= '#h1#' . $title . '#/h1#' . PHP_EOL;
    $content .= PHP_EOL;
    $content .= '#text#' . PHP_EOL . $articleText . PHP_EOL . '#/text#';

    $dirTrash = __DIR__ . '/articles/trash/' . $domain . '/' . $fileName . '_' . $engine . '.txt';
    // Проверка на треш страницу (категории и т.д.)
    if (checkTrashPage($articleText, $title)) {
        sendToLog("Trash page - {$url}", 'error');
        file_force_contents($dirTrash, $content);
        continue;
    }

    // Проверка на количество текста на странице, если меньше к примеру 1000 то не нужно такого текста
    // Записываем его в треш
    if (iconv_strlen($articleText) < 1000) {
        sendToLog('Text count symbols < 1000 - ' . $url, 'error');
        file_force_contents($dirTrash, $content);
        continue;
    }

    // Очищаем некоторые ненужные строчки из статей
    foreach ($clearArticleList as $item) {
        $patterns[] = '/' . $item . '/is';
    }
    $articleText = preg_replace($patterns, '', $articleText);

    // Записываем данные в файл
    $dir = __DIR__ . '/articles/' . $domain . '/' . $fileName . '_' . $engine . '.txt';
    file_force_contents($dir, $content);
    sendToLog('Successfully write text to file - ' . $url, 'success');

    $countUrls--;
}

sendToLog('Parsed: all texts', 'success');

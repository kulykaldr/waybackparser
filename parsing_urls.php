<?php

require_once __DIR__ . '/input_settings.php';

$domainData = selectFromDb($dbUrls, 'domains');
$domainId = $domainData['id'];
$domain = $domainData['data'];

// Если доменов для парсинга нет, то останавливаем работу
if ($domain === false) {
    sendToLog('No domains for parsing', 'error');
    exit();
}

// Delete domain from database
deleteFromDbById($dbUrls, 'domains', $domainId);

// Проверяем наличие в черном списке
if (selectFromDb($dbUrls, 'blacklist', $domain)) {
    sendToLog('Domain in black list', 'error');
    exit();
}

// insert domain to black list
insertToDb($dbUrls, 'blacklist', $domain);

$url = "https://web.archive.org/cdx/search?url={$domain}&matchType=prefix&output=json&fl=original&limit=100000";
$html = getGuzzleHtml($url);

// Если не смогли загрузить и сервер отдает ошибку, то записываем домен
// назад в список для повторной попытки
if (empty($html)) {
    sendToLog('Json html is empty - ' . $url, 'error');
    insertToDb($dbUrls, 'urls', $domain);
    exit();
}

$json_list = json_decode($html);

// Если json пустой вообще значит выкидываем этот логин и останавливаем скрипт
if (!$json_list) {
    sendToLog('Error parsing json', 'error');
    exit();
}

// Очищаем список урлов от дублей и не нужных страниц
foreach ($json_list as $json) {
    $url = trim(urldecode($json[0]));

    if ($url === 'original') {
        continue;
    }

    // delete ? uri parameters
    $url = preg_replace('/\?.*/', '', $url);
    // delete port and / and www. of the end in url
    $url = preg_replace('/(\/$|:\d+|www\.)/i', '', $url);

    // If empty or main domain url continue
    if (empty($url) || empty(parse_url($url, PHP_URL_PATH))) {
        continue;
    }

    // Filter domains with porn keys
    // If detect one url with porno key skip all domain
    if (preg_match('/(' . implode('|', $pornPatternsList) . ')/i', $url)) {
        sendToLog('detect porn key in url - skip domain');
        exit();
    }

    // Поиск на совпадение с паттерном, если присутствует то пропускаем такой урл
    if (preg_match('/(' . implode('|', $patternsList) . ')/i', $url)) {
        continue;
    }

    $urlsList[] = $url;
}

// Проверяем количество страниц для парсинга
// Если список пуст, то пропускаем домен
if (empty($urlsList)) {
    sendToLog('No urls for parsing', 'error');
    exit();
}

// delete duplicates
$urlsList = array_unique($urlsList);

sendToLog('Count pages for parsing: ' . count($urlsList), 'success');

insertToDb($dbUrls, 'urls', $urlsList);
deleteFromDbById($dbUrls, 'domains', $domainId);

sendToLog('Parsing urls successfully ended', 'success');
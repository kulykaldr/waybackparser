<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/GooseClient.php';

ini_set('max_execution_time', 3600);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

date_default_timezone_set('Europe/Kiev');

$dbUrls = new PDO('sqlite:' . __DIR__ . '/lists/urls_list.db');

$clearArticleListPath = __DIR__ . '/lists/clear_article.txt';
$patternsListPath = __DIR__ . '/lists/patterns_url.txt';
$trashPatternsListPath = __DIR__ . '/lists/trash_patterns.txt';
$pornPatternsListPath = __DIR__ . '/lists/porn_urls_pattern.txt';
$searchKeysListPath = __DIR__ . '/lists/search_keys_list.txt';

$clearArticleList = array_map('trim', file($clearArticleListPath));
$patternsList = array_map('trim', file($patternsListPath));
$trashPatternsList = array_map('trim', file($trashPatternsListPath));
$pornPatternsList = array_map('trim', file($pornPatternsListPath));
$searchKeysList = array_map('trim', file($searchKeysListPath));

// Создаем объект guzzle
$guzzle = new \GuzzleHttp\Client([
    'timeout'         => 180,
    'connect_timeout' => 90,
    'http_errors'     => false,
    'allow_redirects' => [
        'max'             => 10,        // allow at most 10 redirects.
        'track_redirects' => true,
    ],
]);

// создаем объект boiler
$boiler = new DotPack\PhpBoilerPipe\ArticleExtractor();

// Создаем объект goose
$goose = new WaybackParser\GooseClient([
    'browser' => [
        'timeout'         => 180,
        'connect_timeout' => 90,
        'http_errors'     => false,
        'allow_redirects' => true,
    ],
]);

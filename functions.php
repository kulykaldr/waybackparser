<?php

use GuzzleHttp\Exception\RequestException;

// Создаем необходисые каталоги на диске для сохранения статей
function file_force_contents($dir, $content, $append = false)
{
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $dir = '';

    // Если контент это массив то разбиваем его с новой строки
    if (is_array($content)) {
        $content = implode(PHP_EOL, $content);
    }

    // Создаем необходимые директории
    foreach ($parts as $part) {
        if (!is_dir($dir .= $part . '/') && !mkdir($dir) && !is_dir($dir)) {
            sendToLog('Error creating folders ', 'error');
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
    }

    // Если указано, что дописывать файл
    $option = 0;
    if ($append === true) {
        $option = FILE_APPEND;
    }

    return file_put_contents($dir . $file, $content . PHP_EOL, $option | LOCK_EX);
}

// Поиск в строке совпадений из массива
function stripos_array($haystack, $needles)
{
    foreach ($needles as $needle) {
        $pos = stripos(strtolower($haystack), strtolower($needle));
        if ($pos !== false) {
            return true;
        }
    }
    return false;
}

// запись в лог
function sendToLog($str, $prefix = '')
{
    global $domain;

    $today = date('d-m-Y');
    $timeNow = date('H:i:s');
    $str = "{$timeNow} - {$domain} - {$str}";
    $prefix = '_' . $prefix;

    echo nl2br($str . PHP_EOL, false);
    file_force_contents(__DIR__ . '/logs/' . $today . $prefix . '_log.txt', $str, true);
}

function getCharset($html)
{
    $charset = null;

    if (preg_match('@<meta.*?charset=["]?([^"\s]+)@im', $html, $matches)) {
        $charset = strtoupper($matches[1]);
    }

    return $charset;
}

function convertToUtf8($html)
{
    if (mb_detect_encoding($html, mb_detect_order(), true) === 'UTF-8') {
        $charset = strtolower(getCharset($html));
        if ($charset !== 'utf-8') {
            $html = preg_replace('/' . $charset . '/im', 'utf-8', $html);
        }
        return $html;
    }

    $charset = getCharset($html);

    if ($charset !== null) {
        $html = preg_replace('@(charset=["]?)([^"\s]+)([^"]*["]?)@im', '$1UTF-8$3', $html);
        $mbHasCharset = in_array($charset, array_map('strtoupper', mb_list_encodings()));

        if ($mbHasCharset) {
            $html = mb_convert_encoding($html, 'UTF-8', $charset);

            // Fallback to iconv if available.
        } elseif (extension_loaded('iconv')) {
            $htmlIconv = iconv($charset, 'UTF-8', $html);

            if ($htmlIconv !== false) {
                $html = $htmlIconv;
            } else {
                $charset = null;
            }
        }
    }

    if ($charset === null) {
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
    }

    return $html;
}

function getH1($html)
{
    $h1 = null;
    preg_match('/<h1.*?>[\w\W]*?<\/h1>/i', $html, $match);
    if (!empty($match)) {
        $h1 = trim(preg_replace('/<.*?>/', '', $match[0]));
    }
    return $h1;
}

function getTitle($html)
{
    $title = null;
    preg_match('/(?<=\<title>)[\w\W]*(?=\<\/title>)/i', $html, $match);
    if (!empty($match)) {
        $title = trim(html_entity_decode($match[0]));
    }
    return $title;
}

function getLang($url)
{
    // Получаем контент страницы
    $html = getGuzzleHtml($url);

    if (empty($html)) {
        return false;
    }

    // Получаем тайтл страницы
    $title = getTitle($html);
    $boiler = new DotPack\PhpBoilerPipe\ArticleExtractor();
    $text = $boiler->getContent($html);

    // Определяем не трешовая ли страница
    if (checkTrashPage($text, $title)) {
        return false;
    }

    // Определяем язык тайтла
    return getGoogleLang($title);
}

function getGoogleLang($text)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://service.m-translate.com/translate');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, '&translate_from=&text=' . $text);
    //Заголовок - не выводим
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // если ведется проверка HTTP User-agent, то передаем один из возможных допустимых вариантов:
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36');
    curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');

    //Поехали!
    //curl_exec ($ch);
    $readyTranslate = curl_exec($ch);
    //Закрываем cURL-сессию
    curl_close($ch);

    $readyTranslate = json_decode($readyTranslate, 1);
    $lang = $readyTranslate['detected_lang'];

    if (empty($lang)) {
        sendToLog('Не найден язык текста', 'error');
        return false;
    }
    sendToLog('язык парсинга - ' . $lang, 'success');

    return $lang;
}

function getGuzzleHtml($url)
{
    global $guzzle;

    try {
        $response = $guzzle->get($url);
    } catch (RequestException $e) {
        sendToLog('Guzzle request error - ' . $url, 'error');
    }

    // Если есть ошибки 400 или еще какие то пропускаем такую страницу
    if ($response->getStatusCode() !== 200) {
        sendToLog('Status code - ' . $response->getStatusCode() . ' - ' . $url, 'error');
        return false;
    }

    $html = $response->getBody()->getContents();
    // Ищем кодировку и меняем при необходимости на utf-8

    return convertToUtf8($html);
}

function checkTrashPage($text, $title)
{
    global $trashPatternsList;

    $trash = false;
    // Проверка на наличие в тайтле названия месяцев - скорее всего это категории
    $titlePatterns = ['отзыв', 'рубрика'];

    // Проверка на треш страницу (категории и т.д.)
    if (stripos_array($text, $trashPatternsList) || stripos_array($title, $titlePatterns)) {
        $trash = true;
    }

    return $trash;
}

function selectFromDb($db, $table, $domain = '')
{
    $sql = 'SELECT * FROM ' . $table . ' LIMIT 1';

    try {
        if ($domain !== '') {
            $sql = 'SELECT * FROM ' . $table . ' WHERE data = :data';
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':data', $domain);
        } else {
            $stmt = $db->prepare($sql);
        }

        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        sendToLog('Error connection to database' . $e->getMessage(), 'error');
    }

    if ($res === false) {
        return false;
    }

    return ['id' => $res['id'], 'data' => $res['data']];
}

function deleteFromDbById($db, $table, $id)
{
    try {
        $sql = 'DELETE FROM ' . $table . ' WHERE id = ' . $id;
        $stmt = $db->query($sql);
        $res = $stmt->execute();
    } catch (PDOException $e) {
        sendToLog('Error connection to database' . $e->getMessage(), 'error');
    }
    return $res;
}

function insertToDb($db, $table, $data)
{
    $sql = 'INSERT INTO ' . $table . ' (data) VALUES (:data)';
    try {
        if (is_array($data)) {
            foreach ($data as $url) {
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':data', $url);
                $res = $stmt->execute();
            }
        } else {
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':data', $data);
            $res = $stmt->execute();
        }
    } catch (PDOException $e) {
        sendToLog('Error connection to database' . $e->getMessage(), 'error');
    }

    return $res;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        try {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        } catch (Exception $e) {
            return '';
        }
    }
    return $randomString;
}